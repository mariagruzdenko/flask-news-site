from datetime import datetime
from marshmallow import Schema, fields, validate, validates, ValidationError
from slugify import slugify

from rest.schemas.fields import ObjectId, DateTime


def get_slug(obj, context):
    return slugify(obj["title"])


class ArticleSchema(Schema):
    id = ObjectId(attribute="_id", dump_only=True)
    title = fields.Str(
        required=True,
        validate=validate.Length(
            max=50,
        ),
    )
    body = fields.Str(required=True)
    category = fields.Str(required=True)
    author = fields.Str(required=True)
    author_id = ObjectId(missing=None)
    created_at = DateTime(missing=datetime.now())
    published_at = DateTime(missing=datetime.now())
    updated_at = DateTime(missing=datetime.now())
    image_id = ObjectId(missing=None)
    slug = fields.Function(get_slug)


class ArticleResponseSchema(Schema):
    id = ObjectId(attribute="_id", dump_only=True)
    title = fields.Str()
    body = fields.Str()
    category = fields.Str()
    author = fields.Str()
    author_id = ObjectId()
    created_at = DateTime()
    published_at = DateTime()
    updated_at = DateTime()
    image_id = ObjectId()
    slug = fields.Str()
    likes_count = fields.Int()
    is_liked = fields.Bool()


class ArticlesResponseSchema(Schema):
    pages = fields.Int()
    page = fields.Int()
    articles = fields.List(fields.Nested(ArticleResponseSchema))


class ArticleUpdateRequestSchema(Schema):
    title = fields.Str(required=True)
    body = fields.Str(required=True)
    category = fields.Str(required=True)
    published_at = DateTime(missing=datetime.now())
    updated_at = DateTime(missing=datetime.now())
    image_id = ObjectId(missing=None)


class ArticleUpdateResponseSchema(Schema):
    title = fields.Str(required=True)
    body = fields.Str(required=True)
    category = fields.Str(required=True)
    author = fields.Str(required=True)
    author_id = ObjectId(missing=None)
    created_at = DateTime(missing=datetime.now())
    published_at = DateTime(missing=datetime.now())
    updated_at = DateTime(missing=datetime.now())
    image_id = ObjectId(missing=None)
    slug = fields.Str()
