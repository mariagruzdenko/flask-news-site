from datetime import datetime
from marshmallow import Schema, fields

from rest.schemas.fields import ObjectId


class UserSchema(Schema):
    id = ObjectId(dump_only=True, attribute="_id")
    username = fields.Str(unique=True)
    password = fields.Str(load_only=True)
    role = fields.Str()
    created_date = fields.DateTime(missing=datetime.now())


class UserRegisterSchema(UserSchema):
    email = fields.Str(unique=True, nullable=False)


class UserPassUpdateSchema(Schema):
    current_pass = fields.Str(required=True)
    new_pass = fields.Str(required=True)
    new_pass_confirm = fields.Str(required=True)
