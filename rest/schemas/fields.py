from datetime import timezone
import bson
import marshmallow as mm


class ObjectId(mm.fields.Str):
    def _deserialize(self, value, attr, data, **kwargs):
        result = super()._deserialize(value, attr, data, **kwargs)
        if result:
            try:
                result = bson.ObjectId(result)
            except bson.errors.InvalidId as err:
                raise mm.ValidationError(str(err))
        return result


class DateTime(mm.fields.DateTime):
    def _serialize(self, value, attr, obj, **kwargs):
        if value is not None:
            value = value.replace(tzinfo=timezone.utc)
        return super()._serialize(value, attr, obj, **kwargs)

    def _deserialize(self, value, attr, data, **kwargs):
        result = super()._deserialize(value, attr, data, **kwargs)
        if result:
            result = result.replace(tzinfo=None)
        return result
