from datetime import datetime
from marshmallow import Schema, fields

from rest.schemas.fields import ObjectId


class BlobMetadataSchema(Schema):
    contentType = fields.Str(required=True)


class BlobSchema(Schema):
    id = ObjectId(attribute="_id")
    length = fields.Int(required=True)
    chunkSize = fields.Int(required=True)
    uploadDate = fields.DateTime(required=True)
    filename = fields.Str(required=True)
    metadata = fields.Nested(BlobMetadataSchema)
    created_at = fields.DateTime(dump_only=True, missing=datetime.now())
    published_at = fields.DateTime(missing=datetime.now())
    updated_at = fields.DateTime(missing=datetime.now())
