from os import environ

# from datetime import timedelta


FLASK_APP = "app"
FLASK_DEBUG = 1
PROPAGATE_EXCEPTIONS = True

API_TITLE = "Stores REST API"
API_VERSION = "v1"
API_BASE = "localhost:5006"
PATH = environ.get("PATH")

OPENAPI_VERSION = "3.0.3"
OPENAPI_URL_PREFIX = "/"
OPENAPI_SWAGGER_UI_PATH = "/swagger-ui"
OPENAPI_SWAGGER_UI_URL = "https://cdn.jsdeliver.net/npm/swagger-ui-dist/"

CORS_ORIGINS = environ.get("CORS_ORIGINS")

MONGO_URI = environ.get("MONGO_URI")

JWT_SECRET_KEY = environ.get("JWT_SECRET_KEY")
# JWT_ACCESS_TOKEN_EXPIRES = timedelta(minutes=1)

CELERY_BROKER = environ.get("CELERY_BROKER")

MAILGUN_URL = environ.get("MAILGUN_URL")
MAILGUN_API_KEY = environ.get("MAILGUN_API_KEY")
MAILGUN_DOMAIN = environ.get("MAILGUN_DOMAIN")

CELERY = {
    "broker": environ.get("CELERY_BROKER"),
    "broker_use_ssl": environ.get("CELERY_BROKER_USE_SSL"),
    "task_default_queue": "celery-priority",
    "task_queue_max_priority": 10,
    "worker_prefetch_multiplier": 1,
    "worker_hijack_root_logger": False,
}

# app.config["JWT_SECRET_KEY"] = secrets.SystemRandom.getrandbits(128)
