"""
This file just contains the blocklist of the JWT tokens. It will be imported by
the app and the logout resources that tokens can be added to the blocklist when
the user logs out.

Reddis can/should be used to store revoked tokens instead of just a regular Python Set.
"""

BLOCKLIST = set()
