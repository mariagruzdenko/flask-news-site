import enum


class Priority(enum.Enum):
    """For use with async tasks."""

    LOW = 1
    NORMAL = 2
    HIGH = 3
