import logging

import click

from pymongo import ASCENDING, DESCENDING

from rest.db import db
from .cli_base import mod

log = logging.getLogger(__name__)


@mod.command()
@click.option("--drop/--no-drop", default=False)
def initdb(drop):
    if drop:
        collections = db.db.list_collection_names()
        for c in collections:
            db.db.drop_collection(c)
    _create_indexes()


@mod.command()
def drop_indexes():
    collections = db.db.list_collection_names()
    for c in collections:
        c.drop_indexes()


@mod.command()
def create_indexes():
    _create_indexes()


def _create_indexes():
    """Creates indexes in a mongoDB"""
    db.db.users.create_index("username", unique=True)

    db.db.articles.create_index("slug", unique=True, name="article_slug")
    db.db.articles.create_index(
        [("author", ASCENDING), ("created_at", DESCENDING)], name="article_author"
    )
    db.db.articles.create_index(
        [("category", ASCENDING), ("created_at", DESCENDING)], name="article_category"
    )
