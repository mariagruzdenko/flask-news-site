from flask import Flask, jsonify
from flask_smorest import Api
from flask_jwt_extended import JWTManager
from celery import Celery
from flask_cors import CORS

from rest.db import db
from rest.ma import ma
from .blueprints import bp_user, bp_article, bp_blob
from rest.models.m_user import UserModel
from rest.blocklist import BLOCKLIST
from rest import cli


def create_app():
    app = Flask(__name__)
    app.config.from_pyfile("settings.py")
    CORS(app)  # will allow all origins ??

    api = Api(app)
    db.init_app(app)
    init_celery(app)
    ma.init_app(app)
    jwt = JWTManager(app)

    @jwt.token_in_blocklist_loader
    def check_if_token_in_blocklist(jwt_header, jwt_payload):
        return jwt_payload["jti"] in BLOCKLIST

    # This callback sets an error message when
    # a token has been revoked(is in the blocklist)
    @jwt.revoked_token_loader
    def revoked_token_callback(jwt_header, jwt_payload):
        return jsonify(
            {
                "description": "The token has been revoked.",
                "error": "token_revoked",
            }
        )

    @jwt.additional_claims_loader
    def add_claims_to_jwt(identity):
        user = UserModel.get_by_id(identity)
        if user["role"] == "admin":
            return {"is_admin": True}
        return {"is_admin": False}

    # Next 3 functions allow to change token error messages
    @jwt.expired_token_loader
    def expired_token_callback(jwt_header, jwt_payload):
        return (
            jsonify({"message": "The token has expired.", "error": "token_expired"}),
            401,
        )

    @jwt.invalid_token_loader
    def invalid_token_callback(error):
        return (
            jsonify(
                {"message": "Signature verification failed.", "error": "invalid_token"},
            ),
            401,
        )

    @jwt.unauthorized_loader
    def missing_token_callback(error):
        return (
            jsonify(
                {
                    "description": "Request does not contain an access token.",
                    "error": "authorization_required",
                }
            ),
            401,
        )

    api.register_blueprint(bp_user.blp, url_prefix="/api")
    api.register_blueprint(bp_article.blp, url_prefix="/api")
    api.register_blueprint(bp_blob.blp, url_prefix="/api")

    app.cli.add_command(cli.mod)

    return app


def init_celery(app):
    celery_config = app.config["CELERY"]
    app.celery = celery = Celery(__name__, broker=celery_config["broker"])

    celery.conf.update(celery_config)

    class ContextTask(celery.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery.Task = ContextTask
    return celery  # is it needed to return a celery instance?
