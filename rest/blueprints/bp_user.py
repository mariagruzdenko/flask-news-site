import os
import requests
from flask.views import MethodView
from flask_smorest import Blueprint, abort
from passlib.hash import pbkdf2_sha256
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    get_jwt_identity,
    jwt_required,
    get_jwt,
)


from rest.schemas.s_user import UserSchema, UserRegisterSchema, UserPassUpdateSchema
from rest.models.m_user import UserModel
from rest.blocklist import BLOCKLIST
from rest import tasks

blp = Blueprint("Users", "users", description="Operations on users")


@blp.route("/users/register")
class UserRegister(MethodView):
    @blp.arguments(UserRegisterSchema)
    @blp.response(201, UserRegisterSchema)
    def post(self, user_data):
        """Create a new user"""
        user = _create_user(**user_data)
        tasks.send_registration_email(user["email"], user["username"])
        return user


@blp.route("/users/login")
class UserLogin(MethodView):
    @blp.arguments(UserSchema)
    def post(self, user_data):
        user = UserModel.find_by_username(user_data["username"])

        if user and pbkdf2_sha256.verify(user_data["password"], user["password"]):
            access_token = create_access_token(identity=str(user["_id"]), fresh=True)
            refresh_token = create_refresh_token(identity=str(user["_id"]))
            return {
                "access_token": access_token,
                "refresh_token": refresh_token,
                "username": user_data["username"],
                "role": user["role"],
                "_id": str(user["_id"]),
            }

        abort(401, message="Invalid credentials")


@blp.route("/users/refresh-token")
class TokenRefresh(MethodView):
    @jwt_required(refresh=True)
    def post(self):
        current_user = get_jwt_identity()
        access_token = create_access_token(identity=current_user, fresh=False)
        return {"access_token": access_token}


@blp.route("/users/logout")
class UserLogout(MethodView):
    @jwt_required()
    def post(self):
        jti = get_jwt()["jti"]
        BLOCKLIST.add(jti)
        return {"message": "Successfully logged out."}


@blp.route("/users")
class User(MethodView):
    @jwt_required()
    @blp.response(200, UserSchema(many=True))
    def get(self):
        """List users"""
        return UserModel.find()

    @jwt_required()
    @blp.arguments(UserRegisterSchema)
    @blp.response(201, UserRegisterSchema)
    def post(self, user_data):
        """Create a new user"""
        user = _create_user(**user_data)
        return user


@blp.route("/users/<username>")
class UserByUsername(MethodView):
    @jwt_required()
    @blp.response(200, UserSchema)
    def get(self, username):
        """Get a user by username"""
        return UserModel.find_by_username(username)

    @jwt_required()
    @blp.arguments(UserPassUpdateSchema)
    @blp.response(200)
    def patch(self, user_data, username):
        if user_data["new_pass"] != user_data["new_pass_confirm"]:
            abort(400, message="Passwords don't match.")

        user = UserModel.find_by_username(username)

        if not user:
            abort(404, messasge="User does not exist.")

        if user and pbkdf2_sha256.verify(user_data["current_pass"], user["password"]):
            password = pbkdf2_sha256.hash(user_data["new_pass"])
            UserModel.update_pass(user["username"], password=password)
        else:
            abort(401, message="Invalid credentials")

        return {"message": "Successfully updated a password."}

    @jwt_required()
    @blp.response(204)
    def delete(self, username):
        jwt = get_jwt()
        if not jwt.get("is_admin"):
            abort(401, message="Admin privilege required.")
        user = UserModel.find_by_username(username)
        if not user:
            abort(404, messasge="User does not exist.")
        user.delete()
        return {"message": "Successfully deleted a user."}


def _create_user(username, email, password, role, created_date):
    user = UserModel.find_by_username(username) or UserModel.find_by_email(email)
    if user:
        abort(
            400,
            message="This username or email already exists. "
            "Can't create a user with this name.",
        )
    else:
        password = pbkdf2_sha256.hash(password)
        user = UserModel.create(
            username=username,
            email=email,
            password=password,
            role=role,
            created_date=created_date,
        )
    return user
