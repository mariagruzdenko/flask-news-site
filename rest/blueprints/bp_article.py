import logging
import math
from bson import ObjectId
from slugify import slugify
from pymongo.errors import DuplicateKeyError

from flask import request
from flask.views import MethodView
from flask_smorest import Blueprint, abort
from flask_jwt_extended import jwt_required, get_jwt_identity

from rest.schemas.s_article import (
    ArticleSchema,
    ArticlesResponseSchema,
    ArticleUpdateRequestSchema,
    ArticleUpdateResponseSchema,
    ArticleResponseSchema,
)
from rest.models.m_article import ArticleModel
from rest import tasks


blp = Blueprint("articles", "articles", description="Operations on news posts")
log = logging.getLogger(__name__)


@blp.route("/home")
class ArticleList(MethodView):
    @blp.response(200, ArticlesResponseSchema)
    @blp.paginate()
    def get(self, pagination_parameters):
        """Get a list of articles (guest user)"""
        return _get_articles(pagination_parameters)


@blp.route("/articles")
class ArticleList(MethodView):
    @jwt_required()
    @blp.response(200, ArticlesResponseSchema)
    @blp.paginate()
    def get(self, pagination_parameters):
        """Get a list of articles (logged in user)"""
        current_user_id = ObjectId(get_jwt_identity())
        return _get_articles(pagination_parameters, user_id=current_user_id)

    @jwt_required()
    @blp.response(201, ArticleSchema)
    @blp.arguments(ArticleSchema, location="form")
    def post(self, article):
        """Create a new article"""
        storage = request.files["file"]
        article["slug"] = slugify(article["title"])
        try:
            result = tasks.create_article(article, storage)
            return result
        except DuplicateKeyError:
            abort(409, message="Article title already exists.")


@blp.route("articles/<string:slug>")
class Article(MethodView):
    @jwt_required()
    @blp.response(200, ArticleResponseSchema)
    def get(self, slug):
        """Get an article by slug"""
        current_user_id = ObjectId(get_jwt_identity())
        article = ArticleModel.get_by_slug(current_user_id, slug)
        if article:
            return article
        abort(404, message="Article id does not exist.")

    @jwt_required()
    @blp.response(204, ArticleUpdateResponseSchema)
    @blp.arguments(ArticleUpdateRequestSchema, location="form")
    def patch(self, article_data, slug):
        """Update an article by ID"""
        storage = request.files["file"]
        current_user = ObjectId(get_jwt_identity())
        article = ArticleModel.get_by_slug(slug)
        if article:
            if current_user != article["author_id"]:
                return abort(403)
            result = tasks.update_article(article, article_data, storage)
            return result
        return abort(404)

    @jwt_required()
    @blp.response(204)
    def delete(self, slug):
        """Delete an article by ID"""
        article = ArticleModel.get_by_slug(slug)
        if article:
            ArticleModel.delete(slug)
            return {"message": "deleted"}
        abort(404, message="Article id does not exist.")


@blp.route("authors/<string:author>/articles")
class ArticleByAuthor(MethodView):
    @jwt_required()
    @blp.response(200, ArticlesResponseSchema)
    @blp.paginate()
    def get(self, author, pagination_parameters):
        """Get a list of articles by author"""
        pagination_parameters.item_count = ArticleModel.count_by_author(author)
        page = pagination_parameters.page
        page_size = pagination_parameters.page_size
        start = (page - 1) * page_size
        articles = ArticleModel.get_documents(
            author=author,
            start=start,
            limit=page_size,
        )
        if articles:
            return {
                "articles": articles,
                "page": page,
                "pages": int(math.ceil(pagination_parameters.item_count / page_size)),
            }
        abort(404, messages="No articles were found.")


@blp.route("categories/<string:category>/articles")
class ArticleByCategory(MethodView):
    @jwt_required()
    @blp.response(200, ArticlesResponseSchema)
    @blp.paginate()
    def get(self, category, pagination_parameters):
        pagination_parameters.item_count = ArticleModel.count_by_category(category)
        page = pagination_parameters.page
        page_size = pagination_parameters.page_size
        start = (page - 1) * page_size
        articles = ArticleModel.get_documents(
            category=category,
            start=start,
            limit=page_size,
        )
        if articles:
            return {
                "articles": articles,
                "page": page,
                "pages": int(math.ceil(pagination_parameters.item_count / page_size)),
            }
        abort(404, messages="No articles were found.")


@blp.route("articles/<string:slug>/like")
class ArticleLikes(MethodView):
    @jwt_required()
    @blp.response(200, ArticleResponseSchema)
    def patch(self, slug):
        current_user_id = ObjectId(get_jwt_identity())
        article = ArticleModel.get_by_slug(user_id=current_user_id, slug=slug)
        if not article:
            abort(404, message="Article does not exist.")

        if article["is_liked"]:
            ArticleModel.unset_like(article_id=article["_id"], user_id=current_user_id)
        else:
            ArticleModel.set_like(article_id=article["_id"], user_id=current_user_id)

        article = ArticleModel.get_by_slug(user_id=current_user_id, slug=slug)
        return article


def _get_articles(pagination_parameters, user_id=None, author=None, category=None):
    pagination_parameters.item_count = ArticleModel.count()
    page = pagination_parameters.page
    page_size = pagination_parameters.page_size
    start = (page - 1) * page_size
    articles = ArticleModel.get_documents(
        start=start,
        limit=page_size,
        user_id=user_id,
        author=author,
        category=category,
    )
    return {
        "articles": articles,
        "page": page,
        "pages": int(math.ceil(pagination_parameters.item_count / page_size)),
    }
