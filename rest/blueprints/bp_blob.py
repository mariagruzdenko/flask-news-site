import logging


from flask import send_file
from flask.views import MethodView
from flask_smorest import Blueprint, abort

from rest.models.m_blob import BlobModel


blp = Blueprint("blobs", "blobs", description="Operations on blobs")
log = logging.getLogger(__name__)


@blp.route("/blobs/<ObjectId:blob_id>")
class Article(MethodView):
    @blp.response(200)
    def get(self, blob_id):
        """Get an image by ID"""
        f = BlobModel.get_by_id(blob_id)
        if f:
            return send_file(
                f,
                mimetype=f.metadata["contentType"],
            )

        return abort(404)
