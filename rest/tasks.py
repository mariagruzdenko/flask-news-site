import requests

# from celery import shared_task

# from rest.celery import Priority
from flask import current_app as app
from rest import models


def _send_simple_message(to, subject, body):
    domain = app.config["MAILGUN_DOMAIN"]
    mailgun_url = app.config["MAILGUN_URL"]
    return requests.post(
        f"{mailgun_url}/{domain}/messages",
        auth=("api", app.config["MAILGUN_API_KEY"]),
        data={
            "from": f"Your name <mailgun@{domain}>",
            "to": [to],
            "subject": subject,
            "text": body,
        },  # we can also send an html emails.
        # The body of the email we can populate with a HTML code
    )


def _send_user_registration_email(email, username):
    return _send_simple_message(
        email,
        "Successfully registered at microblog",
        f"Hi {username}! You've been registered at microblog REST API",
    )


# @shared_task(priority=Priority.LOW.value)
def send_registration_email(email, username):
    _send_user_registration_email(email, username)


# @shared_task(priority=Priority.LOW.value)
def create_article(article, img_storage):
    blob = models.BlobModel.upload(img_storage)
    article["image_id"] = blob._id
    db_article = models.ArticleModel.insert(article)
    return db_article


# @shared_task(priority=Priority.LOW.value)
def update_article(article, article_data, img_storage):
    blob = models.BlobModel.upload(img_storage)
    db_article = models.ArticleModel.update(article["_id"], article_data, blob._id)
    return db_article
