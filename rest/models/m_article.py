from typing import List, Dict
from datetime import datetime
from slugify import slugify

from bson import ObjectId
import pymongo

from rest.db import db


class ArticleModel:
    @classmethod
    def find_all(cls) -> List["ArticleModel"]:
        return db.db.articles.find().sort("created_at", pymongo.DESCENDING)

    @classmethod
    def get_documents(cls, start, limit, user_id=None, author=None, category=None, slug=None) -> List["ArticleModel"]:
        pipeline = []

        match = None
        if author:
            match = {
                "$match": {
                    "author": {"$eq": author}
                }
            }

        if category:
            match = {
                "$match": {
                    "category": {"$eq": category}
                }
            }

        if slug:
            match = {
                "$match": {
                    "slug": {"$eq": slug}
                }
            }

        if match:
            pipeline.append(match)

        add_fields = {
            "$addFields": {
                "likes_count": {"$size": "$likes"},
                "is_liked": {"$in": [user_id, "$likes"]}
            }
        }

        sort = {
            "$sort": {
                "created_at": -1,
            }
        }
        skip = {"$skip": start}
        limit = {"$limit": limit}

        pipeline.extend([add_fields, sort, skip, limit])

        return db.db.articles.aggregate(pipeline)

    @classmethod
    def get_by_id(cls, article_id: str) -> "ArticleModel":
        return db.db.articles.find_one({"_id": ObjectId(article_id)})

    @classmethod
    def get_by_slug(cls, user_id: ObjectId, slug: str) -> "ArticleModel":
        match = {
            "$match": {
                "slug": {"$eq": slug}
            }
        }
        add_fields = {
            "$addFields": {
                "likes_count": {"$size": "$likes"},
                "is_liked": {"$in": [user_id, "$likes"]}
            }
        }
        pipeline = [match, add_fields]
        command_cursor = db.db.articles.aggregate(pipeline)
        if command_cursor.alive:
            return command_cursor.next()
        return None

    @classmethod
    def insert(cls, article: Dict) -> "ArticleModel":
        result = db.db.articles.insert_one(article)
        return db.db.articles.find_one({"_id": result.inserted_id})

    @classmethod
    def update(cls, article_id: str, article_data: Dict, image_id: ObjectId):
        result = db.db.articles.update_one(
            {"_id": ObjectId(article_id)},
            {
                "$set": {
                    "title": article_data["title"],
                    "body": article_data["body"],
                    "category": article_data["category"],
                    "slug": slugify(article_data["title"]),
                    "updated_at": datetime.now(),
                    "image_id": image_id,
                }
            },
        )
        return db.db.articles.find_one({"_id": result.upserted_id})

    @classmethod
    def delete(cls, slug: str):
        return db.db.articles.delete_one({"slug": slug})

    @classmethod
    def count(cls):
        return db.db.articles.count_documents({})

    @classmethod
    def count_by_author(cls, author):
        return db.db.articles.count_documents({"author": author})

    @classmethod
    def count_by_category(cls, category):
        return db.db.articles.count_documents({"category": category})

    @classmethod
    def set_like(cls, article_id, user_id):
        db.db.articles.update_one(
            {"_id": article_id},
            {"$push": {"likes": user_id}},
        )

    @classmethod
    def unset_like(cls, article_id, user_id):
        db.db.articles.update_one(
            {"_id": article_id},
            {"$pull": {"likes": user_id}},
        )
