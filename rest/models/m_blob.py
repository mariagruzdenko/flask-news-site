import logging
import gridfs
from rest.db import db

log = logging.getLogger(__name__)


class BlobModel:
    @classmethod
    def get_by_id(cls, blob_id):
        fs = gridfs.GridFS(db.db)
        return fs.get(file_id=blob_id)

    @classmethod
    def upload(cls, storage):
        """For flask uploads"""
        fs = gridfs.GridFSBucket(db.db)
        fs_id = fs.upload_from_stream(
            storage.filename,
            storage,
            metadata={
                "contentType": storage.mimetype,
            },
        )

        return cls.get_by_id(fs_id)

    @classmethod
    def download(cls, blob_id):
        fs = gridfs.GridFSBucket(db.db)
        grid_out = fs.open_download_stream(blob_id)
        return grid_out.read()

    def delete(self):
        try:
            fs = gridfs.GridFSBucket(db.db)
            fs.delete(self._id)
        except gridfs.errors.NoFile:
            log.warning(f"Trying to delete a blob that is already gone:\n{dict(self)}")

    # def set_article_id(self, article_id):
    #     self.
    #     return self
