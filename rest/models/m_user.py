from typing import List
from rest.db import db
import bson


class UserModel:
    # users = users
    @classmethod
    def find(cls) -> List["UserModel"]:
        return db.db.users.find()

    @classmethod
    def get_by_id(cls, user_id) -> "UserModel":
        return db.db.users.find_one({"_id": bson.ObjectId(user_id)})

    @classmethod
    def find_by_email(cls, email):
        return db.db.users.find_one({"email": email})

    @classmethod
    def find_by_username(cls, username: str) -> "UserModel":
        return db.db.users.find_one({"username": username})

    @classmethod
    def create(cls, **user_data):
        result = db.db.users.insert_one(user_data)
        return db.db.users.find_one({"_id": result.inserted_id})

    @classmethod
    def update_pass(cls, username, **user_data):
        result = db.db.users.update_one(
            {"username": username},
            {
                "$set": {
                    "password": user_data["password"],
                }
            },
        )
        return db.db.users.find_one({"_id": result.upserted_id})
