# flask-news-site



## General Info 

A super simple news site just to play around with flask, flask_smorest, mongoDB 
and React Redux store. This app allows to create, update and like articles, 
filter them by an author and category, watch news feed.

![image info](ui/src/images/1.png)
![image info](ui/src/images/2.png)
![image info](ui/src/images/3.png)