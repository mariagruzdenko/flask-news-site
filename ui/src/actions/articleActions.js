import {
  ARTICLE_CREATE_REQUEST,
  ARTICLE_CREATE_SUCCESS,
  ARTICLE_CREATE_FAIL,

  ARTICLE_LIST_REQUEST,
  ARTICLE_LIST_SUCCESS,
  ARTICLE_LIST_FAIL,

  ARTICLE_DETAILS_REQUEST,
  ARTICLE_DETAILS_SUCCESS,
  ARTICLE_DETAILS_FAIL,

  ARTICLE_UPDATE_REQUEST,
  ARTICLE_UPDATE_SUCCESS,
  ARTICLE_UPDATE_FAIL,

  ARTICLE_TOGGLE_LIKE_REQUEST,
  ARTICLE_TOGGLE_LIKE_SUCCESS,
  ARTICLE_TOGGLE_LIKE_FAIL

} from 'constants/articleConstants'
import axios from 'axios'
import { isTokenFresh } from 'actions/helpers'
import { refreshToken } from 'actions/userActions'

const API_ROOT = process.env.REACT_APP_API_ROOT

export const listArticlesGuest = (keyword = '', page = 1, pageSize = 6) => async (dispatch, getState) => {
  try {
    dispatch({ type: ARTICLE_LIST_REQUEST })
    const url = `${API_ROOT}/api/home?keyword=${keyword}&page=${page}&page_size=${pageSize}`

    const { data } =
        await axios.get(url)

    dispatch({
      type: ARTICLE_LIST_SUCCESS,
      payload: data
    })
  } catch (error) {
    dispatch({
      type: ARTICLE_LIST_FAIL,
      payload: error.response && error.response.data.detail
        ? error.response.data.detail
        : error.message
    })
  }
}

export const listArticles = (keyword = '', page = 1, pageSize = 6) => async (dispatch, getState) => {
  const url = `${API_ROOT}/api/articles?keyword=${keyword}&page=${page}&page_size=${pageSize}`
  await _listArticles(dispatch, getState, url)
}

export const listAuthorArticles = (author, page = 1, pageSize = 6) => async (dispatch, getState) => {
  const url = `${API_ROOT}/api/authors/${author}/articles?page=${page}&page_size=${pageSize}`
  await _listArticles(dispatch, getState, url)
}

export const listCategoryArticles = (category, page = 1, pageSize = 6) => async (dispatch, getState) => {
  const url = `${API_ROOT}/api/categories/${category}/articles?page=${page}&page_size=${pageSize}`
  await _listArticles(dispatch, getState, url)
}

export const listArticleDetails = (slug) => async (dispatch, getState) => {
  try {
    dispatch({ type: ARTICLE_DETAILS_REQUEST })

    const { userLogin: { userInfo } } = getState()

    const fresh = isTokenFresh(userInfo.access_token)

    if (!fresh) {
      await dispatch(refreshToken())
    }

    const config = {
      headers: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${userInfo.access_token}`
      }
    }

    const { data } = await axios.get(`${API_ROOT}/api/articles/${slug}`, config)

    dispatch({
      type: ARTICLE_DETAILS_SUCCESS,
      payload: data
    })
  } catch (error) {
    dispatch({
      type: ARTICLE_DETAILS_FAIL,
      payload: error.response && error.response.data.detail
        ? error.response.data.detail
        : error.message
    })
  }
}

export const createArticle = (article) => async (dispatch, getState) => {
  try {
    dispatch({
      type: ARTICLE_CREATE_REQUEST
    })

    const { userLogin: { userInfo } } = getState()

    const fresh = isTokenFresh(userInfo.access_token)

    if (!fresh) {
      await dispatch(refreshToken())
    }

    const config = {
      headers: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${userInfo.access_token}`
      }
    }

    const { data } = await axios.post(`${API_ROOT}/api/articles`, article, config)

    dispatch({
      type: ARTICLE_CREATE_SUCCESS,
      payload: data
    })
  } catch (error) {
    dispatch({
      type: ARTICLE_CREATE_FAIL,
      payload: error.response && error.response.data.detail
        ? error.response.data.detail
        : error.message
    })
  }
}

export const updateArticle = (articleData, slug) => async (dispatch, getState) => {
  try {
    dispatch({
      type: ARTICLE_UPDATE_REQUEST
    })

    const { userLogin: { userInfo } } = getState()

    const fresh = isTokenFresh(userInfo.access_token)

    if (!fresh) {
      await dispatch(refreshToken())
    }

    const config = {
      headers: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${userInfo.access_token}`
      }
    }

    const { data } = await axios.patch(`${API_ROOT}/api/articles/${slug}`, articleData, config)

    dispatch({
      type: ARTICLE_UPDATE_SUCCESS,
      payload: data
    })
  } catch (error) {
    dispatch({
      type: ARTICLE_UPDATE_FAIL,
      payload: error.response && error.response.data.detail
        ? error.response.data.detail
        : error.message
    })
  }
}

export const likeArticle = (slug) => async (dispatch, getState) => {
  try {
    dispatch({
      type: ARTICLE_TOGGLE_LIKE_REQUEST
    })

    const { userLogin: { userInfo } } = getState()

    const fresh = isTokenFresh(userInfo.access_token)

    if (!fresh) {
      await dispatch(refreshToken())
    }

    const config = {
      headers: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${userInfo.access_token}`
      }
    }

    const { data } = await axios.patch(`${API_ROOT}/api/articles/${slug}/like`, slug, config)

    dispatch({
      type: ARTICLE_TOGGLE_LIKE_SUCCESS,
      payload: data
    })
  } catch (error) {
    dispatch({
      type: ARTICLE_TOGGLE_LIKE_FAIL,
      payload: error.response && error.response.data.detail
        ? error.response.data.detail
        : error.message
    })
  }
}

async function _listArticles (dispatch, getState, url) {
  try {
    dispatch({ type: ARTICLE_LIST_REQUEST })
    const { userLogin: { userInfo } } = getState()

    const fresh = isTokenFresh(userInfo.access_token)

    if (!fresh) {
      await dispatch(refreshToken())
    }

    const config = {
      headers: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${userInfo.access_token}`
      }
    }
    const { data } =
        await axios.get(url, config)
    dispatch({
      type: ARTICLE_LIST_SUCCESS,
      payload: data
    })
  } catch (error) {
    dispatch({
      type: ARTICLE_LIST_FAIL,
      payload: error.response && error.response.data.detail
        ? error.response.data.detail
        : error.message
    })
  }
}
