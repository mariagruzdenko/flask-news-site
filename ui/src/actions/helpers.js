import jwtDecode from 'jwt-decode'

export const isTokenFresh = (accessToken) => {
  const decoded = jwtDecode(accessToken)

  const currentDate = new Date()
  const expirationDate = new Date(decoded.exp * 1000)
  if (expirationDate <= currentDate) {
    return false
  }
  return true
}
