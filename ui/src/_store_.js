import { configureStore } from '@reduxjs/toolkit'

import {
  userDetailsReducer,
  userLoginReducer,
  userRegisterReducer,
  // userUpdateProfileReducer,
  userRefreshTokenReducer

} from 'reducers/userReducers'

import {
  articleCreateReducer,
  articleListReducer,
  articleDetailsReducer,
  myArticleListReducer,
  articleUpdateReducer
} from 'reducers/articleReducers'
import { combineReducers } from 'redux'

const rootReducer = combineReducers({
  userLogin: userLoginReducer,
  userRegister: userRegisterReducer,
  userDetails: userDetailsReducer,
  // userUpdateProfile: userUpdateProfileReducer,
  userRefreshToken: userRefreshTokenReducer,

  articleCreate: articleCreateReducer,
  articleList: articleListReducer,
  articleDetails: articleDetailsReducer,
  myArticleList: myArticleListReducer,
  articleUpdate: articleUpdateReducer
})

export default configureStore({
  reducer: rootReducer
})
