import React from 'react'
import { ThemeProvider } from '@mui/material'
import '@fontsource/montserrat'

import CustomRouter from 'components/Router'

import { theme } from 'theme'

function App () {
  return (
      <ThemeProvider theme={theme}>
          <CustomRouter/>
      </ThemeProvider>
  )
}

export default App
