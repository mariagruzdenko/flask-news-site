import * as React from 'react'
import { Link } from 'react-router-dom'

import Pagination from '@mui/material/Pagination'
import PaginationItem from '@mui/material/PaginationItem'
import Stack from '@mui/material/Stack'

export default function Paginate ({ count, page }) {
  return (
        <Stack spacing={2}>
            <Pagination
                count={count}
                page={page}
                variant="outlined"
                shape="rounded"
                color="primary"
                renderItem={(item) => (
                    <PaginationItem
                        component={Link}
                        to={`${item.page === page ? '' : `?page=${item.page}`}`}
                        {...item}
                    />
                )}
            />
        </Stack>
  )
}
