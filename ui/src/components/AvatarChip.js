import * as React from 'react'
import Chip from '@mui/material/Chip'
import { Link } from 'react-router-dom'
import PersonIcon from '@mui/icons-material/Person'

import { urlFor } from 'routes'

export default function AvatarChip ({ author }) {
  return (
      <Link
          style={{ textDecoration: 'none', color: 'black' }}
          to={urlFor('authorArticles', { author })}>
          <Chip
              sx={{ borderRadius: '8px' }}
              size="small"
              icon={<PersonIcon />}
              label={author} />
      </Link>
  )
}
