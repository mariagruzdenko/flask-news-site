import React from 'react'
import { useSelector } from 'react-redux'
import Chip from '@mui/material/Chip'
import FavoriteIcon from '@mui/icons-material/Favorite'
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder'

export default function LikeChip ({ count, isLiked, onClick }) {
  const { userInfo } = useSelector(state => state.userLogin)

  return (
    userInfo
      ? <Chip
          sx={{ borderRadius: '8px' }}
          icon={isLiked
            ? <FavoriteIcon color={'error'}/>
            : <FavoriteBorderIcon color={'error'}/>}
          label={count}
          onClick={onClick}
      />
      : <Chip
          sx={{ borderRadius: '8px' }}
          icon={<FavoriteBorderIcon color={'error'}/>}
          label={count}
        />
  )
}
