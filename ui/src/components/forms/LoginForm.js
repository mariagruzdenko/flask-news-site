import React, { useState, useEffect } from 'react'
import FormGroup from '@mui/material/FormGroup'
import TextField from '@mui/material/TextField'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import { useDispatch, useSelector } from 'react-redux'
import { useLocation, useNavigate } from 'react-router-dom'
import Grid from '@mui/material/Grid'
import Alert from '@mui/material/Alert'

import { login } from 'actions/userActions'

export default function LoginForm ({ title }) {
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')

  const dispatch = useDispatch()
  const userLogin = useSelector(state => state.userLogin)
  const { error, userInfo } = userLogin
  const navigate = useNavigate()
  const location = useLocation()
  const redirect = location.search ? location.search.split('=')[1] : '/'

  useEffect(() => {
    if (userInfo) {
      navigate(`${redirect}`)
    }
  }, [userInfo, redirect, navigate])

  const handleSubmit = (e) => {
    e.preventDefault()
    dispatch(login(username, password))
  }
  return (
        <Box sx={{
          '& .MuiTextField-root': { m: 1, width: '100%' }
        }}>
            {error ? <Alert severity="error">{error}</Alert> : ''}
            <form onSubmit={handleSubmit}>
                <Grid container spacing={2} alignItems="center" justifyContent="center">
                    <Grid item xs={12}>
                        <FormGroup>
                            <TextField
                                required
                                id="username"
                                label="Username"
                                variant="outlined"
                                value={username}
                                onChange={(e) => setUsername(e.target.value)}
                            />
                            <TextField
                                required
                                id="outlined-password-input"
                                label="Password"
                                type="password"
                                autoComplete="current-password"
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                            />
                        </FormGroup>
                    </Grid>
                    <Grid item xs={6}>
                        <FormGroup>
                            <Button variant="contained" type="submit">{title}</Button>
                        </FormGroup>
                    </Grid>
                </Grid>
            </form>
        </Box>

  )
}
