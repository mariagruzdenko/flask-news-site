import React from 'react'
import { useFormikContext, Form } from 'formik'
import Grid from '@mui/material/Grid'
import FormGroup from '@mui/material/FormGroup'
import Button from '@mui/material/Button'
import { MuiFileInput } from 'mui-file-input'

import FormikTextField from 'components/fields/FormikTextField'

export default function ArticleForm () {
  const formik = useFormikContext()
  const [value, setValue] = React.useState(null)

  const handleChange = (newValue) => {
    setValue(newValue)
    formik.setFieldValue('file', newValue)
  }
  return (
        <Form>
            <Grid container spacing={2} alignItems="center" justifyContent="center">
                <Grid item xs={12}>
                    <FormGroup>
                        <FormikTextField
                            name="title"
                            label="Title"
                        />
                        <FormikTextField
                            name="body"
                            label="Article Body"
                            multiline={true}
                            minRows={10}
                        />
                        <FormikTextField
                            name="category"
                            label="Category"
                        />
                        <MuiFileInput value={value} onChange={handleChange} />
                    </FormGroup>
                </Grid>
                <Grid item xs={6}>
                    <FormGroup>
                        <Button variant="contained" type="submit">Save</Button>
                    </FormGroup>
                </Grid>
            </Grid>
        </Form>
  )
}
