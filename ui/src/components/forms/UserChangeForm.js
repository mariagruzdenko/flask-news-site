import React from 'react'
import Grid from '@mui/material/Grid'
import FormGroup from '@mui/material/FormGroup'
import Button from '@mui/material/Button'
import { Form } from 'formik'

import FormikTextField from 'components/fields/FormikTextField'

export default function UserChangeForm () {
  return (
        <Form>
            <Grid container spacing={2} alignItems="center" justifyContent="center">
                <Grid item xs={12}>
                    <FormGroup>
                        <FormikTextField
                            name="current_pass"
                            label="Current Password"
                            type="password"
                        />
                        <FormikTextField
                            name="new_pass"
                            label="New Password"
                            type="password"
                        />
                        <FormikTextField
                            name="new_pass_confirm"
                            label="New Password Confirm"
                            type="password"
                        />
                    </FormGroup>
                </Grid>
                <Grid item xs={6}>
                    <FormGroup>
                        <Button variant="contained" type="submit">Change</Button>
                    </FormGroup>
                </Grid>
            </Grid>
        </Form>
  )
}
