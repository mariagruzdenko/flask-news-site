import * as React from 'react'
import * as R from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { Link } from 'react-router-dom'

import Card from '@mui/material/Card'
import CardHeader from '@mui/material/CardHeader'
import CardMedia from '@mui/material/CardMedia'
import CardContent from '@mui/material/CardContent'
import CardActions from '@mui/material/CardActions'
import IconButton from '@mui/material/IconButton'
import Typography from '@mui/material/Typography'
import { lightGreen } from '@mui/material/colors'
import EditIcon from '@mui/icons-material/Edit'
import Chip from '@mui/material/Chip'
import Stack from '@mui/material/Stack'

import AvatarChip from 'components/AvatarChip'
import LikeChip from 'components/LikeChip'
import { urlFor } from 'routes'
import { likeArticle } from '../actions/articleActions'

const API_ROOT = process.env.REACT_APP_API_ROOT

export default function ArticleCard ({ article }) {
  const imgUrl = `${API_ROOT}/api/blobs/${article.image_id}`
  const createdAt = new Date(Date.parse(article.created_at)).toLocaleDateString('en-us', { weekday: 'long', year: 'numeric', month: 'short', day: 'numeric' })
  const { userInfo } = useSelector(state => state.userLogin)
  const dispatch = useDispatch()

  const handleLike = () => {
    dispatch(likeArticle(article.slug))
  }

  return (
        <Card sx={{ maxWidth: 345, height: '100%' }}>
            <CardHeader
                title={article.title}
                subheader={createdAt}
            />
            <CardMedia
                component="img"
                height="194"
                image={imgUrl}
                alt="Paella dish"
            />
            <CardContent>
                <Typography variant="body2" color="text.secondary">
                    {article.body.slice(0, 100)} ...
                        {<R.Link
                            style={{ textDecoration: 'none', color: 'blue' }}
                            to={urlFor('article', { slug: article.slug })}>
                            Read more
                        </R.Link>}
                </Typography>
                Author: <AvatarChip author={article.author}/><br/>
                Category: <Chip label={<Link
                                        style={{ textDecoration: 'none', color: 'white' }}
                                        to={urlFor('categoryArticles', { category: article.category })}>
                                            {article.category}
                                        </Link>}
                                color="info"
                                size="small"
                                sx={{ borderRadius: '8px' }}
                                clickable
                            />
            </CardContent>
            <CardActions disableSpacing>
                <Stack direction="row" spacing={1}>
                    <LikeChip onClick={handleLike} count={article.likes_count} isLiked={article.is_liked}/>

                    {
                        userInfo && userInfo.username === article.author
                          ? (
                                <R.Link to={urlFor('updateArticle', { slug: article.slug })} >
                                    <IconButton aria-label="edit">
                                        <EditIcon sx={{ color: lightGreen[500] }}/>
                                    </IconButton>
                                </R.Link>
                            )
                          : ''
                    }
                </Stack>

            </CardActions>
        </Card>
  )
}
