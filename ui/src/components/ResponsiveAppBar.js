import * as React from 'react'
import * as R from 'react-router-dom'
import { useSelector } from 'react-redux'

import AppBar from '@mui/material/AppBar'
import Box from '@mui/material/Box'
import Toolbar from '@mui/material/Toolbar'
import IconButton from '@mui/material/IconButton'
import Typography from '@mui/material/Typography'
import Container from '@mui/material/Container'
import Avatar from '@mui/material/Avatar'
import Tooltip from '@mui/material/Tooltip'

import { urlFor } from 'routes'
import NavDrawer from 'components/NavDrawer'

function ResponsiveAppBar ({ anchor, toggleDrawer }) {
  const userLogin = useSelector(state => state.userLogin)
  const { userInfo } = userLogin
  let username = ''
  if (userInfo) {
    username = userInfo.username
  }

  return (
        <AppBar position="static">
            <Container maxWidth="xl">
                <Toolbar disableGutters>
                    <NavDrawer
                        anchor='left'
                    />
                    <Typography
                        variant="h6"
                        noWrap
                        component="a"
                        href="/"
                        sx={{
                          mr: 2,
                          display: { xs: 'none', md: 'flex' },
                          fontFamily: 'monospace',
                          fontWeight: 700,
                          letterSpacing: '.3rem',
                          color: 'inherit',
                          textDecoration: 'none'
                        }}
                    >
                        LOGO
                    </Typography>

                    {username
                      ? (
                        <Box sx={{ flexGrow: 1, textAlign: 'end' }}>
                            <Tooltip title="Open settings">
                                <IconButton sx={{ p: 0 }}>
                                    <R.Link
                                        sx={{ textDecoration: 'none' }}
                                        to={urlFor('user', { username })}
                                    >
                                        <Avatar
                                            alt={username.toUpperCase()}
                                            src="/static/images/avatar/2.jpg"
                                        />
                                    </R.Link>

                                </IconButton>
                            </Tooltip>
                        </Box>
                        )
                      : (
                        <Box sx={{ flexGrow: 1, textAlign: 'end' }}>
                            <R.Link to={urlFor('login')} style={{ textDecoration: 'none', color: 'white' }}>
                                <b>Sign In</b>
                            </R.Link>
                        </Box>
                        )}

                </Toolbar>
            </Container>
        </AppBar>
  )
}
export default ResponsiveAppBar
