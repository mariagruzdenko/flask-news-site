import React from 'react'
import fp from 'lodash/fp'
import Alert from '@mui/material/Alert'
import Box from '@mui/material/Box'
import ArticleCard from 'components/ArticleCard'
import Paginate from 'components/Paginate'

export default function ArticlesList ({ loading, error, articles, keyword, page, pages }) {
  return (
    loading
      ? 'Loading...'
      : error
        ? <Alert severity="error">{error}</Alert>
        : <>
                <Box display="grid" gridTemplateColumns="repeat(12, 1fr)" gap={2}>
                    {
                        fp.map((article) => (
                            <Box key={article.id} gridColumn="span 4">
                                <ArticleCard article={article}/>
                            </Box>
                        ), articles)
                    }
                    <Box gridColumn="span 12" m="auto">
                        <Paginate count={pages} page={page} keyword={ keyword || '' } />
                    </Box>
                </Box>
            </>

  )
}
