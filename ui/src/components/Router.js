import * as React from 'react'
import { HashRouter as Router, Routes, Route } from 'react-router-dom'

import routes from 'routes'
import HomePage from 'pages/HomePage'
import LoginPage from 'pages/LoginPage'
import ArticleCreatePage from 'pages/ArticleCreatePage'
import ArticleDetailPage from 'pages/ArticleDetailPage'
import ArticlesListPage from 'pages/ArticlesListPage'
import ArticleEditPage from 'pages/ArticleEditPage'
import UserDetailPage from 'pages/UserDetailPage'
import UserEditPage from 'pages/UserEditPage'
import MainPage from 'pages/MainPage'

export default function CustomRouter () {
  return (
        <Router>
            <Routes>
                 <Route path={routes.main.path} element={<MainPage/>} exact />
                <Route path={routes.home.path} element={<HomePage/>} exact />
                <Route path={routes.login.path} element={<LoginPage/>} />
                {/* <Route path='/register'  element={<RegisterPage />} /> */}
                 <Route path={routes.user.path} element={<UserDetailPage />} />
                 <Route path={routes.userChangePassword.path} element={<UserEditPage />} />
                <Route path={routes.article.path} element={<ArticleDetailPage/>} exact />
                <Route path={routes.authorArticles.path} element={<ArticlesListPage byAuthor/>} exact />
                <Route path={routes.categoryArticles.path} element={<ArticlesListPage byCategory/>} exact />
                <Route path={routes.createArticle.path} element={<ArticleCreatePage />} />
                <Route path={routes.updateArticle.path} element={<ArticleEditPage />} />
            </Routes>
        </Router>
  )
}
