import * as React from 'react'
import * as R from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'

import Box from '@mui/material/Box'
import Drawer from '@mui/material/Drawer'
import List from '@mui/material/List'
import Divider from '@mui/material/Divider'
import ListItem from '@mui/material/ListItem'
import ListItemButton from '@mui/material/ListItemButton'
import ListItemIcon from '@mui/material/ListItemIcon'
import ListItemText from '@mui/material/ListItemText'
import HomeIcon from '@mui/icons-material/Home'
import ExitToAppIcon from '@mui/icons-material/ExitToApp'
import EditIcon from '@mui/icons-material/Edit'
import AccountBoxIcon from '@mui/icons-material/AccountBox'
import * as I from '@mui/icons-material'
import IconButton from '@mui/material/IconButton'
import LoginIcon from '@mui/icons-material/Login'
import LockOpenIcon from '@mui/icons-material/LockOpen'

import { logout } from 'actions/userActions'
import { urlFor } from 'routes'

export default function NavDrawer ({ anchor }) {
  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false
  })

  const userLogin = useSelector(state => state.userLogin)
  const { userInfo } = userLogin
  let username = ''
  if (userInfo) {
    username = userInfo.username
  }

  const dispatch = useDispatch()

  const toggleDrawer = (anchor, open) => (event) => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return
    }

    setState({ ...state, [anchor]: open })
  }

  const logoutHandler = (e) => {
    e.preventDefault()
    dispatch(logout())
  }

  const MenuItemLink = ({ route, params, icon, title }) => {
    return (
            <List>
                <ListItem key={title} to={urlFor(route, params)} disablePadding component={R.Link}>
                    <ListItemButton>
                        <ListItemIcon>
                            {icon}
                        </ListItemIcon>
                        <ListItemText primary={title} />
                    </ListItemButton>
                </ListItem>
            </List>
    )
  }

  const MenuItem = ({ icon, title, onClick }) => {
    return (
            <List>
                <ListItem key={title} disablePadding onClick={onClick}>
                    <ListItemButton>
                        <ListItemIcon>
                            {icon}
                        </ListItemIcon>
                        <ListItemText primary={title} />
                    </ListItemButton>
                </ListItem>
            </List>
    )
  }

  const list = (anchor) => (
        <Box
            sx={{ width: anchor === 'top' || anchor === 'bottom' ? 'auto' : 250 }}
            role="presentation"
            onClick={toggleDrawer(anchor, false)}
            onKeyDown={toggleDrawer(anchor, false)}
        >
            {
                username
                  ? <MenuItemLink
                        route='home'
                        icon={<HomeIcon/>}
                        title="Home"
                        />
                  : <MenuItemLink
                        route='main'
                        icon={<HomeIcon/>}
                        title="Home"
                    />
            }
            <Divider />
            {
                username
                  ? (
                    <MenuItemLink
                        route='authorArticles'
                        params={{ author: userInfo.username }}
                        icon={<AccountBoxIcon/>}
                        title="My Articles"
                    />
                    )
                  : ''
            }
            {
                username
                  ? (
                    <MenuItemLink
                        route='createArticle'
                        icon={<EditIcon/>}
                        title="Create Article"
                    />
                    )
                  : ''
            }

            {
                username
                  ? (
                    <MenuItem
                        icon={<ExitToAppIcon/>}
                        title="Log Out"
                        onClick={logoutHandler}
                    />
                    )
                  : (
                    <>
                        <MenuItemLink
                            route='login'
                            icon={<LoginIcon/>}
                            title="Log In"
                        />
                        <MenuItemLink
                            route='register'
                            icon={<LockOpenIcon/>}
                            title="Register"
                        />
                    </>
                    )
            }
        </Box>
  )

  return (
        <div>
            <React.Fragment key={anchor}>
                <IconButton onClick={toggleDrawer(anchor, true)} color="inherit">
                    <I.Menu />
                </IconButton>
                <Drawer
                    anchor={anchor}
                    open={state[anchor]}
                    onClose={toggleDrawer(anchor, false)}
                >
                    {list(anchor)}
                </Drawer>
            </React.Fragment>

        </div>
  )
}
