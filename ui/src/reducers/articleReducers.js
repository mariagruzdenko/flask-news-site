import fp from 'lodash/fp'
import {
  ARTICLE_LIST_REQUEST,
  ARTICLE_LIST_SUCCESS,
  ARTICLE_LIST_FAIL,

  ARTICLE_DETAILS_REQUEST,
  ARTICLE_DETAILS_SUCCESS,
  ARTICLE_DETAILS_FAIL,

  ARTICLE_CREATE_REQUEST,
  ARTICLE_CREATE_SUCCESS,
  ARTICLE_CREATE_FAIL,

  ARTICLE_CREATE_RESET,

  ARTICLE_UPDATE_REQUEST,
  ARTICLE_UPDATE_SUCCESS,
  ARTICLE_UPDATE_FAIL,
  ARTICLE_UPDATE_RESET,

  ARTICLE_TOGGLE_LIKE_REQUEST,
  ARTICLE_TOGGLE_LIKE_SUCCESS,
  ARTICLE_TOGGLE_LIKE_FAIL

} from 'constants/articleConstants'

export const articleCreateReducer = (state = {}, action) => {
  switch (action.type) {
    case ARTICLE_CREATE_REQUEST:
      return {
        loading: true
      }
    case ARTICLE_CREATE_SUCCESS:
      return {
        loading: false,
        success: true,
        article: action.payload
      }
    case ARTICLE_CREATE_FAIL:
      return {
        loading: false,
        error: action.payload
      }

    case ARTICLE_CREATE_RESET:
      return {}

    default:
      return state
  }
}

export const articleUpdateReducer = (state = { }, action) => {
  switch (action.type) {
    case ARTICLE_UPDATE_REQUEST:
      return { loading: true }

    case ARTICLE_UPDATE_SUCCESS:
      return { loading: false, success: true, article: action.payload }

    case ARTICLE_UPDATE_FAIL:
      return { loading: false, error: action.payload }

    case ARTICLE_UPDATE_RESET:
      return {}

    default:
      return state
  }
}

const reduceArticle = (state, data) =>
  fp.set(['articles', data.id], data, state)

export const articleListReducer = (state = { articles: [] }, action) => {
  let articles = null
  switch (action.type) {
    case ARTICLE_LIST_REQUEST:
      return { loading: true, articles: [] }

    case ARTICLE_LIST_SUCCESS:
      articles = action.payload.articles
      articles = articles.reduce((acc, elem) => {
        acc[elem.id] = elem
        return acc
      }, {})
      return {
        loading: false,
        articles,
        page: action.payload.page,
        pages: action.payload.pages
      }

    case ARTICLE_LIST_FAIL:
      return { loading: false, error: action.payload }

    case ARTICLE_TOGGLE_LIKE_REQUEST:
      return { loading: true, ...state }

    case ARTICLE_TOGGLE_LIKE_SUCCESS: {
      state = reduceArticle(state, action.payload)
      return {
        loading: false,
        articles: { ...state.articles },
        page: state.page,
        pages: state.pages
      }
    }

    case ARTICLE_TOGGLE_LIKE_FAIL:
      return { loading: false, error: action.payload }

    default:
      return state
  }
}

export const articleDetailsReducer = (state = { article: { reviews: [] } }, action) => {
  switch (action.type) {
    case ARTICLE_DETAILS_REQUEST:
      return { loading: true, ...state }

    case ARTICLE_DETAILS_SUCCESS:
      return { loading: false, article: action.payload }

    case ARTICLE_DETAILS_FAIL:
      return { loading: false, error: action.payload }

    default:
      return state
  }
}
