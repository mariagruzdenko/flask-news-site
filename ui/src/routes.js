import { useLocation } from 'react-router-dom'
import fp from 'lodash/fp'
import { reverse } from 'named-urls'

const routeList = [
  { name: 'main', path: '/' },
  { name: 'home', path: '/home' },
  { name: 'login', path: '/login' },
  { name: 'logout', path: '/logout' },
  { name: 'register', path: '/register' },
  // { name: 'profile', path: '/profile' },
  { name: 'users', path: '/users' },
  { name: 'user', path: '/users/:username' },
  { name: 'userChangePassword', path: '/users/:username/change-pass' },
  { name: 'articles', path: '/articles' },
  { name: 'authorArticles', path: '/authors/:author/articles' },
  { name: 'categoryArticles', path: '/categories/:category/articles' },
  { name: 'article', path: '/articles/:slug' },
  { name: 'createArticle', path: '/articles/create' },
  { name: 'updateArticle', path: '/articles/:slug/edit' },
  { name: 'setLikeArticle', path: '/articles/:slug/set-like' },
  { name: 'unsetLikeArticle', path: '/articles/:slug/unset-like' }
]

export const routes = fp.transform(
  (acc, r) => {
    acc[r.name] = r
  },
  {},
  routeList
)

export const useQuery = () => new URLSearchParams(useLocation().search)

export const urlFor = (route, params) => {
  try {
    const { path } = routes[route]
    return reverse(path, params)
  } catch (e) {
    console.error('Error looking up route', route, params)
    throw e
  }
}

export default routes
