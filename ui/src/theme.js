import { createTheme } from '@mui/material/styles'

export const theme = createTheme({
  typography: {
    useNextVariants: true,
    fontFamily: '"Montserrat"'
  },
  // palette: {
  //     og: {
  //         pcc: '#97CC04',
  //         hubspot: '#FF5C35',
  //         white: '#FFFFFF',
  //         upload: '#FFC135',
  //     },
  // },
  overrides: {},
  constants: {}
})
