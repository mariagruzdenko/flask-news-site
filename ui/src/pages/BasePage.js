import React from 'react'
import { Paper } from '@mui/material'
import { makeStyles } from 'tss-react/mui'

import ResponsiveAppBar from '../components/ResponsiveAppBar'
import Footer from '../components/Footer'

const useStyles = makeStyles()(theme => ({
  root: {
    '& h1': {
      ...theme.typography.h1,
      margin: 0
    },
    '& h2': theme.typography.h2,
    '& h3': theme.typography.h3,
    '& h4': {
      ...theme.typography.h4,
      margin: 0
    },
    '& h5': theme.typography.h5,
    '& h6': theme.typography.h6,
    '& p': theme.typography.body1,
    height: '100%',
    '& a.green-link': {
      color: '#459e76'
    }
  },
  contentContainer: {
    padding: theme.spacing(2),
    paddingBottom: theme.spacing(8)
  },
  contentArea: {
    // maxWidth: 1280,
    maxWidth: 1400,
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  contentContainerWide: {
    padding: theme.spacing(2)
  },
  spacer: {
    height: theme.spacing(4)
  },
  text_center: {
    textAlign: 'center'
  }
}))

export default function BasePage ({ title, children }) {
  const { classes } = useStyles()

  return (
        <Paper className={classes.root}>
            <ResponsiveAppBar
                anchor='left'
            />
            <div className={classes.contentContainer}>
                <div className={classes.contentArea}>
                    <h2 className={classes.text_center}>{title}</h2>
                    {children}
                </div>
            </div>
            <Footer />
        </Paper>
  )
}
