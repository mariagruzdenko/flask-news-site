import React, { useEffect } from 'react'

import { useDispatch, useSelector } from 'react-redux'
import { useNavigate, useParams } from 'react-router-dom'
import { Formik } from 'formik'
import { useSnackbar } from 'notistack'

import routes from 'routes'
import BasePage from 'pages/BasePage'
import ArticleForm from 'components/forms/ArticleForm'
import { listArticleDetails, updateArticle } from 'actions/articleActions'
import { ARTICLE_UPDATE_RESET } from 'constants/articleConstants'

export default function ArticleEditPage () {
  const { error: updateError, success: updateSuccess } = useSelector(state => state.articleUpdate)
  const { error, loading, article } = useSelector(state => state.articleDetails)
  const { userInfo } = useSelector(state => state.userLogin)
  const { enqueueSnackbar } = useSnackbar()

  const dispatch = useDispatch()
  const navigate = useNavigate()
  const params = useParams()

  useEffect(() => {
    if (!userInfo) {
      navigate(routes.login.path)
    } else {
      dispatch(listArticleDetails(params.slug))
    }

    if (updateSuccess) {
      navigate(`/article/${article.slug}`) // change to urlFor
      dispatch({ type: ARTICLE_UPDATE_RESET })
    }
    if (updateError) {
      enqueueSnackbar(updateError)
    }
  }, [dispatch, params, navigate, updateSuccess, updateError, userInfo, error])

  const patchArticle = (values) => {
    dispatch(updateArticle(
      {
        title: values.title,
        category: values.category,
        body: values.body,
        file: values.file
      }, params.slug))
  }

  const validate = (values) => {
    const errors = {}
    if (!values.title) {
      errors.title = 'Required'
    } else if (
      (values.title.length <= 3)
    ) {
      errors.title = 'Title must be at least 3 characters long'
    }
    if (!values.body) {
      errors.body = 'Required'
    } else if (
      (values.body.length <= 10)
    ) {
      errors.body = 'Article body must be at least 10 characters long'
    }
    if (!values.category) {
      errors.category = 'Required'
    }
    return errors
  }
  return (
        <BasePage title="Edit an Article">
            { loading
              ? ''
              : (
                <Formik
                    initialValues={{
                      title: article.title,
                      body: article.body,
                      category: article.category,
                      file: ''
                    }}
                    validate={validate}
                    onSubmit={patchArticle}
                >
                    <ArticleForm />
                </Formik>
                )}

        </BasePage>
  )
}
