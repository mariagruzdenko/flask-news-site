import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate, useParams } from 'react-router-dom'

import Container from '@mui/material/Container'
import Paper from '@mui/material/Paper'
import Typography from '@mui/material/Typography'
import Alert from '@mui/material/Alert'

import BasePage from 'pages/BasePage'
import { listArticleDetails } from 'actions/articleActions'
import routes, { urlFor } from 'routes'
import LikeChip from '../components/LikeChip'
import * as R from 'react-router-dom'
import IconButton from '@mui/material/IconButton'
import EditIcon from '@mui/icons-material/Edit'
import { lightGreen } from '@mui/material/colors'
import Stack from '@mui/material/Stack'

const API_ROOT = process.env.REACT_APP_API_ROOT

export default function ArticleDetailPage () {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const params = useParams()

  const { error, loading, article } = useSelector(state => state.articleDetails)
  const { userInfo } = useSelector(state => state.userLogin)

  const createdAt = new Date(Date.parse(article.created_at)).toLocaleDateString('en-us', { weekday: 'long', year: 'numeric', month: 'short', day: 'numeric' })
  const imgUrl = `${API_ROOT}/api/blobs/${article.image_id}`

  useEffect(() => {
    if (!userInfo) {
      navigate(routes.login.path)
    } else {
      dispatch(listArticleDetails(params.slug))
    }
  }, [dispatch, params])

  return (
        <BasePage>
            <Container>
                { loading
                  ? 'Loading...'
                  : error
                    ? <Alert severity="error">{error}</Alert>
                    : <Paper elevation={3} sx={{ padding: 2 }}>
                            <Typography variant="h4" gutterBottom>
                                {article.title}
                            </Typography>
                            <p>{createdAt}</p>
                            <img src={imgUrl} alt=""/>
                            <p>{article.body}</p>
                            <Stack direction="row" spacing={1}>
                              <LikeChip count={article.likes_count} isLiked={article.is_liked}/>

                              {
                                userInfo && userInfo.username === article.author
                                  ? (
                                        <R.Link to={urlFor('updateArticle', { slug: article.slug })} >
                                          <IconButton aria-label="edit">
                                            <EditIcon sx={{ color: lightGreen[500] }}/>
                                          </IconButton>
                                        </R.Link>
                                    )
                                  : ''
                              }
                            </Stack>
                        </Paper>
                }
            </Container>
        </BasePage>
  )
}
