import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate, useSearchParams } from 'react-router-dom'

import BasePage from 'pages/BasePage'
import { listArticles } from 'actions/articleActions'
import ArticlesList from 'components/ArticlesList'
import routes from 'routes'

export default function HomePage () {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const articleList = useSelector(state => state.articleList)
  const { error, loading, articles, page, pages } = articleList
  const { userInfo } = useSelector(state => state.userLogin)

  const [searchParams] = useSearchParams()

  let keyword = searchParams.get('keyword')
  keyword = keyword || ''

  let searchPage = searchParams.get('page')
  searchPage = searchPage || 1

  useEffect(() => {
    if (!userInfo) {
      navigate(routes.login.path)
    } else {
      dispatch(listArticles(keyword, searchPage))
    }
  }, [dispatch, keyword, searchPage])

  return (
            <BasePage
                title="All Articles"
            >
              <ArticlesList
                  loading={loading}
                  error={error}
                  articles={articles}
                  keyword={keyword}
                  page={page}
                  pages={pages}
              />
            </BasePage>
  )
}
