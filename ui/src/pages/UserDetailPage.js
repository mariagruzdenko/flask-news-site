import React, { useEffect } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { useSelector } from 'react-redux'
import Grid from '@mui/material/Grid'
import Card from '@mui/material/Card'
import CardContent from '@mui/material/CardContent'
import Box from '@mui/material/Box'
import Avatar from '@mui/material/Avatar'

import BasePage from './BasePage'
import { Item } from '../components/Item'
import routes, { urlFor } from '../routes'

export default function UserDetailPage () {
  const { userInfo } = useSelector(state => state.userLogin)
  const navigate = useNavigate()

  useEffect(() => {
    if (!userInfo) {
      navigate(routes.login.path)
    }
  }, [userInfo, navigate])

  return (
        <BasePage
            title={ userInfo ? `${userInfo.username}'s Profile` : '' }
        >
            {
                userInfo
                  ? <Box sx={{ flexGrow: 1 }}>
                    <Grid container spacing={2} alignItems='center' justifyContent='center'>
                        <Grid item xs={4}>
                            <Item>
                                <Card sx={{ minWidth: 200 }} variant={'outlined'}>
                                    <CardContent>
                                        <Avatar
                                            alt={userInfo.username.toUpperCase()}
                                            src='/static/images/avatar/2.jpg'
                                        />
                                        <p><b>id: </b>{userInfo._id}</p>
                                        <p><b>role: </b>{userInfo.role}</p>

                                        <Link to={urlFor('userChangePassword', { username: userInfo.username })}>
                                            Change Password
                                        </Link>
                                    </CardContent>
                                </Card>
                            </Item>
                        </Grid>
                    </Grid>
                </Box>
                  : ''
            }

        </BasePage>
  )
}
