import React, { useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { Formik } from 'formik'
import { useSnackbar } from 'notistack'

import routes from '../routes'
import BasePage from './BasePage'
import ArticleForm from '../components/forms/ArticleForm'
import { ARTICLE_CREATE_RESET } from '../constants/articleConstants'
import { createArticle } from '../actions/articleActions'

export default function ArticleCreatePage () {
  const articleCreate = useSelector(state => state.articleCreate)
  const { article, error, success } = articleCreate
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const { userInfo } = useSelector(state => state.userLogin)
  const { enqueueSnackbar } = useSnackbar()

  useEffect(() => {
    if (!userInfo) {
      navigate(routes.login.path)
    }

    if (success) {
      navigate(`/article/${article.slug}`) // add success page & articles slug
      dispatch({ type: ARTICLE_CREATE_RESET })
    }
    if (error) {
      enqueueSnackbar(error)
    }
  }, [success, error, navigate, dispatch, article, userInfo, navigate])

  const postArticle = (values) => {
    dispatch(createArticle(
      {
        title: values.title,
        category: values.category,
        author: userInfo.username,
        author_id: userInfo._id,
        body: values.body,
        file: values.file,
        likes: []
      }))
  }
  const validate = (values) => {
    const errors = {}
    if (!values.title) {
      errors.title = 'Required'
    } else if (
      (values.title.length <= 3)
    ) {
      errors.title = 'Title must be at least 3 characters long'
    }
    if (!values.body) {
      errors.body = 'Required'
    } else if (
      (values.body.length <= 10)
    ) {
      errors.body = 'Article body must be at least 10 characters long'
    }
    if (!values.category) {
      errors.category = 'Required'
    }
    return errors
  }

  return (
        <BasePage
            title="Create an Article"
        >
            <Formik
                initialValues={{
                  title: '',
                  body: '',
                  category: '',
                  file: ''
                }}
                validate={validate}
                onSubmit={postArticle}
            >
                <ArticleForm />
            </Formik>

        </BasePage>
  )
}
