import * as React from 'react'
import Grid from '@mui/material/Grid'
import Box from '@mui/material/Box'
import Card from '@mui/material/Card'
import CardContent from '@mui/material/CardContent'

import BasePage from './BasePage'
import LoginForm from '../components/forms/LoginForm'
import { Item } from '../components/Item'
import { urlFor } from '../routes'

export default function LoginPage () {
  return (
        <BasePage
            title="Log In Page"
        >
            <Box sx={{ flexGrow: 1 }}>
                <Grid container spacing={2} alignItems="center" justifyContent="center">
                    <Grid item xs={4}>
                        <Item>
                            <Card sx={{ minWidth: 200 }} variant={'outlined'}>
                                <CardContent>
                                    <LoginForm title={'Log In'}/>
                                </CardContent>
                            </Card>
                        </Item>
                    </Grid>
                    <Grid item xs={12}>
                        Don&apos;t have an account yet? Create one&nbsp;
                          <a href={urlFor('register')}>here</a>
                    </Grid>
                </Grid>
            </Box>
        </BasePage>
  )
}
