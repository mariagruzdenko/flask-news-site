import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useSearchParams, useParams, useNavigate } from 'react-router-dom'

import BasePage from 'pages/BasePage'
import { listAuthorArticles, listCategoryArticles } from 'actions/articleActions'
import ArticlesList from 'components/ArticlesList'
import routes from 'routes'

export default function ArticlesListPage ({ byAuthor, byCategory, guest }) {
  let title = ''
  const { error, loading, articles, page, pages } = useSelector(state => state.articleList)

  const userLogin = useSelector(state => state.userLogin)
  const { userInfo } = userLogin
  let username
  if (userInfo) {
    username = userInfo.username
  }

  const params = useParams()
  const navigate = useNavigate()
  const dispatch = useDispatch()

  const [searchParams] = useSearchParams()

  let keyword = searchParams.get('keyword')
  keyword = keyword || ''

  let searchPage = searchParams.get('page')
  searchPage = searchPage || 1

  useEffect(() => {
    if (!userInfo) {
      navigate(routes.login.path)
    }

    if (byAuthor) {
      dispatch(listAuthorArticles(params.author, searchPage))
    }
    if (byCategory) {
      dispatch(listCategoryArticles(params.category, searchPage))
    }
  }, [dispatch, keyword, searchPage, params.author, params.category, userInfo])

  if (byAuthor) {
    title = username === params.author ? 'My Articles' : `${params.author}'s Articles`
  }

  if (byCategory) {
    title = `Articles in ${params.category} Category`
  }

  return (
        <BasePage
            title={title}
        >
          <ArticlesList
              loading={loading}
              error={error}
              articles={articles}
              keyword={keyword}
              page={page}
              pages={pages}
          />
        </BasePage>
  )
}
