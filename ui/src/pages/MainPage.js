import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useSearchParams } from 'react-router-dom'

import BasePage from 'pages/BasePage'
import ArticlesList from 'components/ArticlesList'
import { listArticlesGuest } from 'actions/articleActions'

export default function MainPage () {
  const dispatch = useDispatch()
  const articleList = useSelector(state => state.articleList)
  const { error, loading, articles, page, pages } = articleList

  const [searchParams] = useSearchParams()

  let keyword = searchParams.get('keyword')
  keyword = keyword || ''

  let searchPage = searchParams.get('page')
  searchPage = searchPage || 1

  useEffect(() => {
    dispatch(listArticlesGuest(keyword, searchPage))
  }, [dispatch, keyword, searchPage])

  return (
        <BasePage title="All Articles">
          <ArticlesList
              loading={loading}
              error={error}
              articles={articles}
              keyword={keyword}
              page={page}
              pages={pages}
          />
        </BasePage>
  )
}
