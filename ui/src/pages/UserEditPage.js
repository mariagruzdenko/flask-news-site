import { React, useEffect } from 'react'
import { Formik } from 'formik'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { useSnackbar } from 'notistack'

import BasePage from './BasePage'
import UserChangeForm from '../components/forms/UserChangeForm'
import { updateUserPass } from '../actions/userActions'
import routes, { urlFor } from '../routes'
import { Alert } from '@mui/material'

export default function UserEditPage () {
  const userPassUpdate = useSelector(state => state.userPassUpdate)
  const { error, loading, success } = userPassUpdate
  const { enqueueSnackbar } = useSnackbar()
  const { userInfo } = useSelector(state => state.userLogin)

  const navigate = useNavigate()
  const dispatch = useDispatch()

  useEffect(() => {
    if (!userInfo) {
      navigate(routes.login.path)
    }
    if (success) {
      navigate(urlFor('user', { username: userInfo.username }))
    }
  }, [success, navigate])

  const patchUser = (values) => {
    if (values.new_pass !== values.new_pass_confirm) {
      enqueueSnackbar("New passwords don't match. Can't update a password.")
    } else {
      dispatch(updateUserPass({
        current_pass: values.current_pass,
        new_pass: values.new_pass,
        new_pass_confirm: values.new_pass_confirm
      }))
    }
  }

  return (
        <BasePage title="Change Password">
            { loading
              ? ''
              : error
                ? <Alert severity="error">{error}</Alert>
                : '' }
            <Formik
                initialValues={{
                  current_pass: '',
                  new_pass: '',
                  new_pass_confirm: ''
                }}
                onSubmit={patchUser}
            >
                <UserChangeForm />
            </Formik>
        </BasePage>
  )
}
