import React from 'react'
import ReactDOM from 'react-dom/client'
import { Provider } from 'react-redux'
import 'css/index.css'
import App from 'App'
import store from 'store'
import { SnackbarProvider } from 'notistack'

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
  <React.StrictMode>
      <Provider store={store}>
          <SnackbarProvider>
              <App />
          </SnackbarProvider>
      </Provider>
  </React.StrictMode>
)
