import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'

import {
  userDetailsReducer,
  userLoginReducer,
  userRegisterReducer,
  userRefreshTokenReducer,
  userPassUpdateReducer

} from 'reducers/userReducers'

import {
  articleCreateReducer,
  articleListReducer,
  articleDetailsReducer,
  articleUpdateReducer
} from 'reducers/articleReducers'

const reducer = combineReducers({
  userLogin: userLoginReducer,
  userRegister: userRegisterReducer,
  userDetails: userDetailsReducer,
  userPassUpdate: userPassUpdateReducer,
  userRefreshToken: userRefreshTokenReducer,
  articleCreate: articleCreateReducer,
  articleList: articleListReducer,
  articleDetails: articleDetailsReducer,
  articleUpdate: articleUpdateReducer
})

const userInfoFromStorage = localStorage.getItem('userInfo')
  ? JSON.parse(localStorage.getItem('userInfo'))
  : null

const initialState = {
  userLogin: { userInfo: userInfoFromStorage }
}

const middleware = [thunk]

const store = createStore(reducer, initialState, composeWithDevTools(applyMiddleware(...middleware)))

export default store
